To launch the following application,

1) Download WebCrawler.jar from the Downloads tab on https://bitbucket.org/fmontei/webcrawler/downloads
2) Type the following into the command line:

	java -jar WebCrawler.jar arg0 arg1 arg2
	where:
	arg0 is a URL
	arg1 is crawl depth (limit to under 2)
	arg2 is sub-directory for saving data; "" for current working directory
	
	Example: 
	java -jar WebCrawler.jar 0 http://www.clemson.edu/ ""
	java -jar WebCrawler.jar 1 http://www.clemson.edu/ temp
