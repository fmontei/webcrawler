package WebCrawler;

public class DownloadDepository {

	private static DownloadDepository singleton = null;
	private static Saveable[] VisualData = new Saveable[] {
		
		new WebImage(), new WebFile()
	};
	
	protected DownloadDepository() {
		
	}
	
	DownloadDepository getInstance() {
		
		if (singleton == null) {
			
			singleton = new DownloadDepository();
		}
		
		return (singleton);
	}
	
	void downloadData() {
		
		for (Saveable currentSaveData : DownloadDepository.VisualData)
			
			currentSaveData.saveData();
	}
}
