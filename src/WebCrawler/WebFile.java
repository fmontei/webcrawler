package WebCrawler;

import java.util.ArrayList;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class WebFile 
extends SaveableContent 
implements WebElement, Saveable {
	
	private static final String fileTemplate = "file";
	private static final String fileDirectory = "/files/";
	private static final String[] fileEndings = 
		{".doc", ".pdf", ".txt", ".docx", ".xls", ".xlsx", ".ppt", ".pptx",
		 ".zip", ".rar", ".7z"};

	public void fetchData(Document doc) {
		
		Elements files = doc.select("a[href]");
		
		for (Element file : files) {
			
			// Iterates over array of file endings to find valid file formats
			for (String fileEnding : fileEndings) {
			
				if (file.toString().contains(fileEnding)) {
					
					if (!WebPage.stringData.contains(file)) {
						
						WebPage.stringData.add(file.attr("href"));
						WebPage.files.add(file);
						WebCrawler.fileCount++;
					}
				}
			}
		}
	}

	public void saveData() {
	
		ArrayList<Element> fileData = new WebFile().getData();
		super.saveData(fileData, fileTemplate, fileDirectory);
	}
	
	public ArrayList<Element> getData() {
		
		return WebPage.files;
	}
}
