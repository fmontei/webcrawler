/* Name: Felipe Monteiro (The Swayze) and Mike Dozier
 * Date: September 30, 2013
 * Class: CpSc 215
 * Professor: Dr. Hochrine
 * 
 * Project 1: WebCrawler - parses images, files and webpages and saves
 * 			               images and files to the appropriate directory
 * 
 * Note: to run the program, enter the following commands below:
 * javac -cp .:jsoup-1.7.2.jar cu/cs/cpsc215/project1/WebCrawler.java
 * java -cp .:jsoup-1.7.2.jar cu.cs.cpsc212.project1.WebCrawler http://
 * www.clemson.edu 1 /home/fmontei/Downloads/ <--- need that last slash
 */

package WebCrawler;

import java.io.*;
import java.util.*;
import org.jsoup.nodes.Element;

public class WebCrawler {
	
	protected static int URLCount;
	protected static int imageCount;
	protected static int fileCount;
	
	protected static String targetDirectory;
	protected static PrintStream toFile = null;
	protected static PrintStream errorLog = null;
	
	public static void main(String[] args) {

		if (args.length != 3) {
			System.out.println("Usage: provide 3 arguments of the form"
					+ "arg0 arg1 arg2 where\n \targ0 = initial web address\n "
					+ "\targ1 = crawl depth\n "
					+ "\targ2 = name of sub-directory for saving files");
			System.out.println("To save files to current working directory,"
					+ " type \"\" for arg2");
			System.exit(1);
		}

		final int crawlDepth = Integer.parseInt(args[1]);
		
		String directory = System.getProperty("user.dir");
		
		if (args[2].isEmpty() || args[2].equals("")) {
			WebCrawler.targetDirectory = directory;
		}
		else {
			if (!args[2].startsWith("\\"))
				args[2] = "\\" + args[2];
			if (!args[2].endsWith("\\"))
				args[2] = args[2] + "\\";
			WebCrawler.targetDirectory = directory + args[2];
		}
		
		/* Create sub-directory for fetched data to be saved in */
		if (!args[2].equals("") && !(new File(directory).mkdir())) {
			System.out.println("Could not create the subdirectory " + args[2]
				+ " in the file system " + directory);
			Integer input = -1;
			while (input < 1 || input > 3) {
				System.out.println("Choose another save directory: ");
				System.out.println("1: " + directory);
				System.out.println("2: Specify your own directory: ");
				System.out.println("3: Quit program");
				
				Scanner scan = new Scanner(System.in);
				try {
					input = scan.nextInt();
				} catch(Exception e) {
					input = -1;
				}
				
				switch(input) {
					case 1:
						WebCrawler.targetDirectory = directory;
						System.out.println("Crawl directory set to " +
							directory);
						break;
					case 2:
						boolean valid = false;
						File dir = null;
						
						while (!valid) {
							scan = new Scanner(System.in);
							dir = new File(scan.nextLine());
							if (dir.isDirectory()) valid = true;
							else System.out.println
								("Invalid directory. Try again.");
						}
						
						WebCrawler.targetDirectory = dir.toPath().toString();
						System.out.println("Crawl directory set to " +
							dir.toPath());
						break;
					case 3:
						System.exit(1);
						break;
				}
			}
				
		}
		
		WebCrawler webCrawler = new WebCrawler();
		WebPage targetWebPage = new WebPage();
		
		System.out.println("Target directory = "  + 
				WebCrawler.targetDirectory);
		
		try {
			toFile = new PrintStream(WebCrawler.targetDirectory + 
					"/output.txt");
			errorLog = new PrintStream(WebCrawler.targetDirectory + 
					"/error_log.txt");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		System.out.println("Crawl Depth 0 . . .");
		targetWebPage.crawl(args[0]);
		
		for ( int i = 1; i <= crawlDepth; ++i ) {
			
			ArrayList<Element> iterator = 
					new ArrayList<Element>(WebPage.webpages);
			WebPage.webpages.clear();
			
			System.out.println("Crawl Depth " + i + " . . .");
			
			for (Element nextWebpage : iterator) {
				
				final String nextURL = nextWebpage.attr("href");
				targetWebPage.crawl(nextURL);
			}
		}
		
		// Prints a list of all links, images and files fetched to a file
		webCrawler.printContents();
		
		// Singleton class
		DownloadDepository downloadDepository = new DownloadDepository();
		downloadDepository.getInstance();
		downloadDepository.downloadData();
		
		WebCrawler.toFile.flush();
		WebCrawler.errorLog.flush();
		System.out.println("Crawling complete. " +
				"See output.txt for results.");
	}
	
	void printContents() {
		
		int count = 0;
		
		for (String key : WebPage.stringData) {
			
			if (key == "")
				continue;
			
			WebCrawler.toFile.println(count + ": " + key);
			count++;
		}
		
		WebCrawler.toFile.println("====================================");
		WebCrawler.toFile.println("\nTotal number of webpages fetched: " + 
				WebCrawler.URLCount + "\n");
		WebCrawler.toFile.println("Total number of images fetched: " + 
				WebCrawler.imageCount + "\n");
		WebCrawler.toFile.println("Total number of files fetched: " + 
				WebCrawler.fileCount + "\n");
		WebCrawler.toFile.println("====================================");
	}
}
