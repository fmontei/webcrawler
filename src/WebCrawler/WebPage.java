package WebCrawler;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashSet;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class WebPage implements WebElement {
	
	// All the URLs are saved to the HashSet called stringData: prevents 
	// the program from revisiting that URL
	protected static HashSet<String> stringData = new HashSet<String>();
	protected static ArrayList<Element> webpages = 
			new ArrayList<Element>(); 
	protected static ArrayList<Element> images = new ArrayList<Element>();
	protected static ArrayList<Element> files = new ArrayList<Element>();
	
	private static WebElement[] Data = new WebElement[3];
	
	static {
		
		// Files must be fetched first, because otherwise they will not be
		// inserted into webpageData, since web pages and web files both start
		// with <a href> 
		WebPage.Data[0] = new WebImage();
		WebPage.Data[1] = new WebFile();
		WebPage.Data[2] = new WebPage();
	}	
	
	void crawl(String targetWebpage) {
		
		Document doc = null;
		
		try {
			doc = Jsoup.connect(targetWebpage).userAgent("Mozilla").get();
		} catch (IllegalArgumentException i) {
			i.printStackTrace(WebCrawler.errorLog);
		} catch (MalformedURLException m) {
			m.printStackTrace(WebCrawler.errorLog);
		} catch (Exception e) {
			e.printStackTrace(WebCrawler.errorLog);
		}
		
		if (doc == null)
			return;

		for (WebElement currentData : WebPage.Data) {
			
			currentData.fetchData(doc);
		}
	}
	
	public void fetchData(Document doc) {
		
		Elements links = doc.select("a[href]");
		
		for (Element link : links) {
 
			if (!link.attr("href").contains("http://")) {
				
				continue;
			}
			
			if (!WebPage.stringData.contains(link)) {
				
				WebPage.stringData.add(link.attr("href"));
				WebPage.webpages.add(link);
				
				WebCrawler.URLCount++;
			}
		}
	}

	public ArrayList<Element> getData() {
		
		return WebPage.webpages;
	}
}
