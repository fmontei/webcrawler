package WebCrawler;

import java.util.ArrayList;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class WebImage 
extends SaveableContent
implements WebElement, Saveable {
	
	protected static final String imageTemplate = "image";
	private static final String imageDirectory = "/images/";
	
	public WebImage() {  
		
	}
	
	public void fetchData(Document doc) {
		
		Elements images = doc.select("img[src~=(?i)\\.(png|jpe?g|gif|bmp|img)]");
	
		for (Element image : images) {
			
			if(!image.attr("src").startsWith("http://") || 
					image.attr("src").contains("href")) {
				
				continue;
			}
			
			if (!WebPage.stringData.contains(image)) {
				
				WebPage.stringData.add(image.attr("src"));
				WebPage.images.add(image);
				WebCrawler.imageCount++;
			}
		}
	}
	
	public void saveData() {
		
		ArrayList<Element> imageData = new WebImage().getData();
	    super.saveData(imageData, imageTemplate, imageDirectory);
	}
	
	public ArrayList<Element> getData() {
		
		return WebPage.images;
	}
}
