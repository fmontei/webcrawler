package WebCrawler;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.ArrayList;

import org.jsoup.nodes.Element;

public abstract class SaveableContent implements Saveable {
	
	public void saveData(ArrayList<Element> fileData, 
			String fileTemplate,
			String subDirectory) {
	    
		File directory = 
				new File(WebCrawler.targetDirectory + subDirectory);
		
	    if(!directory.exists()) {
	    	
	    	if (directory.mkdir()) {
	    		
	    		System.out.println("New " + fileTemplate +
	    				" directory created.");
	    	}
	    }
	    
	    // Deletes all files from the subdirectory if it already exists
	    else 
        	for (File file : directory.listFiles()) file.delete(); 
	    
	    System.out.println("Saving " + fileTemplate + "s" + 
	    		" to directory . . .");
	    
	    String source;
	    int increment = 0;
	    
		for (Element file : fileData) {
			
			if (fileTemplate == WebImage.imageTemplate) {
				
		    	source = file.attr("src");
			}
			
			else {
				
				source = file.attr("href");
			}
			
			try {
				final String fileEnding =
						source.substring(source.lastIndexOf('.'), 
						source.lastIndexOf('.') + 4);
				final String fileName = fileTemplate + increment + fileEnding;

				URL url = new URL(source);
				InputStream is = url.openStream();
				OutputStream os = new FileOutputStream(fileName);
		
				byte[] b = new byte[2048];
				int length;
		
				while ((length = is.read(b)) != -1) {
					
					os.write(b, 0, length);
				}
				
				is.close();
				os.close();
				
				File currentFile = new File(fileName);
				
				// Deletes files under 100 bytes
				if (currentFile.length() < 100)
					currentFile.delete();
				
				else
					increment++;
	
				if (currentFile.exists()) {
					
				currentFile.renameTo(new File(WebCrawler.targetDirectory
							+ subDirectory + fileName));
				}
					
			} catch (Exception e) {
				e.printStackTrace(WebCrawler.errorLog);
			}
		}
	}
}
