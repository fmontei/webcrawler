package WebCrawler;

import java.util.ArrayList;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

public interface WebElement {

	ArrayList<Element> getData();
	void fetchData(Document doc);
}
